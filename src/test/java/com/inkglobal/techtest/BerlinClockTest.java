package com.inkglobal.techtest;

import static org.junit.Assert.assertEquals;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class BerlinClockTest {

    private BerlinClock berlinClock = new BerlinClock(); 
    
    @Rule
    public ExpectedException exception = ExpectedException.none();
    
    @Test
    public void testToggleHourLamps(){
        String hourLamps = berlinClock.toggleHourLamps(13);
        assertEquals(hourLamps, "RROO RRRO");
    }
    
    @Test
    public void testtoggleBlink(){
        String seconds = berlinClock.toggleSecondsLamp(01);
        assertEquals(seconds, "O");
    }
    
    @Test
    public void testToggleMinuteLamps(){
        String minuteLamps = berlinClock.toggleMinuteLamps(59);
        assertEquals(minuteLamps, "YYRYYRYYRYY YYYY");
    }
    @Test
    public void testAll() throws Exception {
        assertEquals(berlinClock.switchLamps("00:00:00"), "Y OOOO OOOO OOOOOOOOOOO OOOO");
        assertEquals(berlinClock.switchLamps("13:17:01"), "O RROO RRRO YYROOOOOOOO YYOO");
        assertEquals(berlinClock.switchLamps("23:59:59"), "O RRRR RRRO YYRYYRYYRYY YYYY");
        assertEquals(berlinClock.switchLamps("24:00:00"), "Y RRRR RRRR OOOOOOOOOOO OOOO");
    }
    
    @Test
    public void testError() throws Exception {
        exception.expect(Exception.class);
        berlinClock.switchLamps("44:10:66");
    }

   
}
