package com.inkglobal.techtest;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author omogbehing.
 * Representation of the Berlin clock
 */
public class BerlinClock {

    private final static char OFF    = 'O';
    private final static char RED    = 'R';
    private final static char YELLOW = 'Y';
    private Pattern pattern;
    private Matcher matcher;
    private static final String TIME24HOURS_PATTERN = "(([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9])|(24:00:00)";
    
    public BerlinClock(){
        pattern = Pattern.compile(TIME24HOURS_PATTERN);
    }

    /**
     * The published method for this class
     * @param time
     * @return
     * @throws Exception
     */
    public String switchLamps(final String time) throws Exception {
        final int[] timeArr = extractTimeData(time);
        StringBuilder lamps = new StringBuilder();
        lamps.append( toggleSecondsLamp(timeArr[2])).append(" ").append(toggleHourLamps(timeArr[0])).append(" ").append(toggleMinuteLamps(timeArr[1]));
        return lamps.toString();
    }
    
    /**
     * Validates time fragment ensuring it meets the requirement defined in regex
     * @param time
     * @return
     */
    private boolean validate(final String time){        
                   matcher = pattern.matcher(time);
                   return matcher.matches();            
                }

    /**
     * Extracts and validates time string
     * Creates an array by splitting the delimited time string
     * @param timeString
     * @return
     * @throws Exception
     */
    private int[] extractTimeData(final String timeString) throws Exception {
        if(!validate(timeString)){
            throw new Exception("Invalid input");
        }
        int[] time = new int[3];
        int i = 0;
        for (String str : timeString.split(":")) {
            time[i++] = Integer.parseInt(str);
        }
        return time;
    }

    /**
     * Based on hour value, switches Red lamps on or off
     * @param hour
     * @return
     */
    protected String toggleHourLamps(int hour) {
        return processTimeData(hour, 4, RED);
    }
    
    /**Based on minute value, switches Red lamps on or off
     * @param minutes
     * @return
     */
    protected String toggleMinuteLamps(int minutes) {
        return processTimeData(minutes, 11, YELLOW);
    }
    
    /**
     * Preprocesses time data before toggling lamps - Potential method for abstraction
     * First rows values are in multiple of 5
     * Second rows carry single value and the remainder of multiple of 5
     * @param timeFragment
     * @param firstRowCount - number of lamps in first row (second row is fixed = 4)
     * @param fragmentColour - RED or YELLOW depending on the time fragment
     * @return
     */
    private String processTimeData(int timeFragment, int firstRowCount, char fragmentColour){
        final int firstRow = timeFragment / 5;
        final int secondRow = timeFragment % 5;
        StringBuilder minLamps = new StringBuilder();
        minLamps.append(toggleLamps(firstRow, firstRowCount, fragmentColour, true)).append(" ").append(toggleLamps(secondRow, 4, fragmentColour, false));
        return minLamps.toString();
    }

    /**
     * Actual business logic - Potential method for abstraction
     * @param value - Number of lamps to be switched on
     * @param count - Total number of lamps
     * @param colour - Colour to be switched on
     * @param partitionRed - Applicable to minute time fragment and row 1
     * @return
     */
    private String toggleLamps(final int value, final int count, final char colour, final boolean partitionRed) {
        StringBuilder rowBuilder = new StringBuilder();
        for (int i = 1; i <= count; i++) {
            if (i <= value) {
                if (partitionRed && i % 3 == 0) {
                    rowBuilder.append(RED);
                }
                else {
                    rowBuilder.append(colour);
                }
            }
            else {
                rowBuilder.append(OFF);
            }
        }
        return rowBuilder.toString();
    }

    /**
     * Toggles seconds lamp on and off
     * @param secondsFragment
     * @return
     */
    protected String toggleSecondsLamp(final int secondsFragment) {
        return String.valueOf((secondsFragment % 2) == 0 ? YELLOW : OFF);
    }
}